const express = require('express');

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  res.send('Lista de usuarios en el sistema');
}

function index(req, res, next) {
  res.send(`Usuario del sistema con in ID =  ${req.params.id}`);
}

function create(req, res, next){
  const name = req.body.name;
  const lastName = req.body.lastName;
  res.send(`Crear un usuario nuevo con nombre ${name} y apellido ${lastName}`);
}

function replace(req, res, next) {
  res.send(`Reemplazo un usuario con ID = ${req.params.id} por otro`);
}

function edit(req, res, next) {
  res.send(`Reemplazo las propiedades del usuario con ID = ${req.params.id} por otras`);
}

function destroy(req, res, next) {
  res.send(`Elimino un usuario con ID = ${req.params.id}`);
}

module.exports = {
  list, index, create, replace, edit, destroy
}
