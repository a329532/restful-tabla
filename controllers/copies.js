const express = require('express');
const { Copy } = require('../db');

// RESTFULL => GET, POST, PUT, PATCH, DELETE = Modelo
// representacion de una estructura de datos
function list(req, res, next) {
  Copy.findAll({include:['bookings']})
    .then(objects => res.json(objects))
    .catch(err => res.json(err));
}

function index(req, res, next) {
  const id = req.params.id;
  Copy.findByPk(id)
  .then(object => res.json(object))
  .catch(err => res.json(err));
}

function create(req, res, next){
  const number = req.body.number;
  const format = req.body.format;
  const status = req.body.status;
  const movieId = req.body.movieId;

  let copy = new Object({
    number:number,
    format:format,
    status:status,
    movieId:movieId
  });

  Copy.create(copy)
    .then(obj => res.json(obj))
    .catch(err => res.json(err));
}

function replace(req, res, next) {
  const id = req.params.id;
  Copy.findByPk(id)
    .then((object) => {
      const number = req.body.number ? req.body.number : "";
      const format = req.body.format ? req.body.format : "";
      const status = req.body.status ? req.body.status : 0;
      const movieId = req.body.movieId ? req.body.movieId : 0;
      object.update({number:number, format:format, status:status, movieId:movieId})
        .then(copy => res.json(copy));
      res.json(object);
    })
    .catch(err => res.json(err));
}

function edit(req, res, next) {
  const id = req.params.id;
  Copy.findByPk(id)
    .then((object) => {
      const number = req.body.number ? req.body.number : object.number;
      const format = req.body.format ? req.body.format : object.format;
      const status = req.body.status ? req.body.status : object.status;
      const movieId = req.body.movieId ? req.body.movieId : object.movieId;
      object.update({number:number, format:format, status:status, movieId:movieId})
        .then(copy => res.json(copy));
      res.json(object);
    })
    .catch(err => res.json(err));
}

function destroy(req, res, next) {
  const id = req.params.id;
  Copy.destroy({where: {id:id}})
    .then(obj => res.json(obj))
    .catch(err => res.json(err));
}

module.exports = {
  list, index, create, replace, edit, destroy
}
