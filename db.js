const Sequelize = require('sequelize');
const directorModel = require('./models/director')
const genreModel = require('./models/genre');
const movieModel = require('./models/movie');
// 1) db name 2) user 3) password 4)obj config

const sequelize = new Sequelize('videoclub','root','abcd1234',{
    host: 'localhost', //direccion de rdbms
    dialect: 'mysql'
});

const Director = directorModel(sequelize, Sequelize);
const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);

Genre.hasMany(Movie,{as:'movies'});

Movie.belongsTo(Genre,{as:'genre'});

sequelize.sync({
    force: true
}).then(()=>{
    console.log("Base de datos actualizada correctamente");
});

module.exports = {Director,Genre};